#!/bin/bash
set -ex;

prefix=7.1r4

docker build --no-cache=true -t surface0/php-fpm:${prefix} -f ./Dockerfile . && \
docker build --no-cache=true -t surface0/php-fpm:${prefix}-cakephp -f ./Dockerfile-cakephp . && \
docker build --no-cache=true -t surface0/php-fpm:${prefix}-wordpress -f ./Dockerfile-wordpress .

echo Finished.