# Description

Docker file for custom build Alpine & PHP-FPM images.

# Build

```
./build.sh
```

# Image Tags

|name|from|specification|
|---|---|---|
|{version}|{version}-fpm-alpine(official)|Join to gid=1000 group "www-data" user<br>with sendmail(external smtp)|
|{version}-cakephp|{version}|Install PHP extensions for Cakephp3|
|{version}-wordpress|{version}|Cutomized for WordPress|

# Environments

|name|default|
|---|---|
|SSMTP_ROOT|root@localdomain|
|SSMTP_AUTH_USER|hoge@gmail.com|
|SSMTP_AUTH_PASS|password|
