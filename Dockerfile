FROM php:7.1-fpm-alpine

LABEL maintainer "surface0 <admin+docker@rainorshine.asia>"

RUN set -ex; \
    addgroup -g 1000 web; \
    addgroup www-data web; \
    apk add --no-cache ssmtp mailx gettext; \
    docker-php-ext-install -j4 opcache; \
    rm -rf /tmp/pear; \
    echo "php_flag[display_errors] = off" >> /usr/local/etc/php-fpm.d/www.conf; \
    echo "php_admin_value[error_log] = /var/log/fpm-php.www.log" >> /usr/local/etc/php-fpm.d/www.conf; \
    echo "php_admin_flag[log_errors] = on" >> /usr/local/etc/php-fpm.d/www.conf; \
    echo "php_admin_value[memory_limit] = 64M" >> /usr/local/etc/php-fpm.d/www.conf;

# ssmtp config
ENV SSMTP_ROOT="root@localdomain" \
    SSMTP_AUTH_USER="example@gmail.com" \
    SSMTP_AUTH_PASS="password"

COPY ./ssmtp/ssmtp.conf.template /etc/ssmtp/ssmtp.conf.template

# overwrite entrypoint
COPY docker-php-entrypoint /usr/local/bin